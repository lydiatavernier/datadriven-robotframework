*** Settings ***
Library		fizzBuzz.py

*** Test Cases ***
FizzBuzz
	[Template]		monTemplate
	${3}				Fizz

	${5}				Buzz
		
	${15}				FizzBuzz
		
	${7}				7
	
*** Keywords ***
montemplate
	[Arguments]		${num}	${expectedString}
	${res}	fizzBuzz	${num}
	Should Be Equal As Strings	${res}	${expectedString}