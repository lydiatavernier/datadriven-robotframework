*** Settings ***
Library		fizzBuzz.py
Test Template   monTemplate

*** Test Cases ***		                                NUM			EXPECTEDSTRING
FizzBuzzz retourne Fizz pour la valeur 3				${3}				Fizz

FizzBuzzz retourne Buzz pour la valeur 5				${5}				Buzz
	
FizzBuzzz retourne FizzBuzz pour la valeur 15			${15}				FizzBuzz
	
FizzBuzzz retourne 7 pour la valeur 7   				${7}				7
	
*** Keywords ***
montemplate
	[Arguments]		${num}	${expectedString}
	${res}	fizzBuzz	${num}
	Should Be Equal As Strings	${res}	${expectedString}