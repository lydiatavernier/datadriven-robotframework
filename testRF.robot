*** Settings ***
Library		fizzBuzz.py

*** Test Cases ***
FizzBuzzz retourne Fizz pour la valeur 3
	${res1}		fizzBuzz	${3}
	Should Be Equal As Strings	${res1}		Fizz
	Log To Console	${res1}
	
FizzBuzzz retourne Buzz pour la valeur 5
	${res1}		fizzBuzz	${5}
	Should Be Equal As Strings	${res1}		Buzz
	Log To Console	${res1}
	
FizzBuzzz retourne FizzBuzz pour la valeur 15
	${res1}		fizzBuzz	${15}
	Should Be Equal As Strings	${res1}		FizzBuzz
	Log To Console	${res1}
	
FizzBuzzz retourne 7 pour la valeur 7
	${res1}		fizzBuzz	${7}
	Should Be Equal As Strings	${res1}		7
	Log To Console	${res1}

