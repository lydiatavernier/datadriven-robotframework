*** Settings ***
Test Template    FizzBuzzz should return the right value fizzbuzz

*** Test Cases ***
FizzBuzzz retourne 7 pour la valeur 7
	FizzBuzzz should return the right value fizzbuzz    7   7

FizzBuzzz retourne fizz pour la valeur 3
	FizzBuzzz should return the right value fizzbuzz       3    fizz

*** Keywords ***
FizzBuzzz should return the right value fizzbuzz
	[Arguments] ${input}   ${expectedOutput}
	${output}		fizzBuzz	${input}
	Should Be Equal As Strings	${output}   ${expectedOutput}
	Log To Console	${output}


